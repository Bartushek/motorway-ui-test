const cacheable = require('cacheable-response')

const CACHE_TTL = 900000; // 15 min
const STALE_TTL = 60000; // 1 min

module.exports = cacheable({
  ttl: CACHE_TTL,
  staleTtl: STALE_TTL,
  get: async ({ req, res, fn }) => ({ data: await fn(req, res) }),
  send: ({ res, data }) => res.status(200).json(data),
})
