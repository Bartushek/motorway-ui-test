const app = require("express")();
const morgan = require("morgan");

const imagesApi = require("./api/images");
const getSharpImage = require("./api/getSharpImage");

app.use(morgan("tiny"));
app.use("/images", imagesApi);
app.use("/sharp-image", getSharpImage);


app.listen(5000, () => {
  process.stdout.write("Server is available on http://localhost:5000/\n");
});

// - remove setTimeout :)
// - use static file serving
// - monitor RAM usage
// - cache warmup
// - cache testing - what will happen if server returns error
// - images.json have data mismatch with likes
