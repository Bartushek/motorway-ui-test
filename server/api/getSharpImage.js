// This is only to get resized images locally, generally this should be done within infrastructure (as a Lambda caching on S3 bucket, or use some other CDN tools for images)

const fs = require("fs");
const http = require("http");
const https = require("https");
const Stream = require("stream").Transform;
const Sharp = require("sharp");
const express = require("express");
const router = express.Router();
const path = require('path');

const DOWNLOAD_DIR = path.resolve(__dirname, '../../media');

if (!fs.existsSync(DOWNLOAD_DIR)){
    fs.mkdirSync(DOWNLOAD_DIR);
}

const getFileName = (url) => url.split("/").pop();
const extractUrlData = (url) => {
  // should be regex
  const size = Number(url.split("-w").pop().split(".")[0]) || 1000;
  const realUrl = url.replace(/-w[0-9]*\./, ".");
  const fileName = getFileName(realUrl);

  return { fileName, url: realUrl, size };
};

const downloadImage = (data, callback) => {
  const { url, fileName } = data;
  const client = url.indexOf('https') === 0 ? https : http;

  // should be wrapped with Promise
  try {
    client
      .request(url, (res) => {
        var data = new Stream();

        res.on("data", function (chunk) {
          data.push(chunk);
        });

        res.on("end", function () {
          fs.writeFileSync(path.resolve(DOWNLOAD_DIR, fileName), data.read());
          callback();
        });
      })
      .end();
  } catch (e) {
    // invalid callback
    callback();
  }
};

const readImage = (fileName) => {
  return new Promise((res, rej) => {
    fs.readFile(path.resolve(DOWNLOAD_DIR, fileName), function (err, data) {
      if (err) return rej(err);
      res(data);
    });
  });
};

const readAndServe = ({ fileName, size, res }) => {
  return readImage(fileName)
    .then(async (data) => {
      // should read format from filetype
      // result of Sharp should be cached
      return Sharp(data).resize(size).toFormat("webp").toBuffer();
    })
    .then((data) => {
      res.contentType(fileName);
      res.send(data);
    });
};

router.get("/", async (req, res) => {
  const { url } = req.query;
  const data = extractUrlData(url);

  // Callback hell - could be improved
  readAndServe({ ...data, res }).catch(() => {
    downloadImage(data, () => {
      readAndServe({ ...data, res }).catch(() => {
        // could be much more different errors
        res.status(404).json("Missing file");
      });
    });
  });
});

module.exports = router;
