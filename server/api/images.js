const express = require("express");
const images = require("../resources/images.json");
const cache = require("../cache");

const router = express.Router();

const randomInterval = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

router.get("/", (req, res) => {
  cache({
    req,
    res,
    fn: async ({ query }) => {
      const { limit, from = 0 } = query;
      const parsedFrom = parseInt(from);
      const parsedLimit = parseInt(limit);
      const i = limit ? images.slice(parsedFrom, parsedFrom + parsedLimit) : images;

      return await new Promise((resolve) => {
        setTimeout(() => {
          resolve(i);
        }, randomInterval(500, 1500));
      }) 
    },
  })
}
);

module.exports = router;
