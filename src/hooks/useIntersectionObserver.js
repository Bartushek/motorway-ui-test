import { useEffect, useMemo } from "react";

const createObserver = (fn, options) =>
  new IntersectionObserver(
    (entries) => {
      entries.forEach(({ target, intersectionRatio }) => {
        if (intersectionRatio > 0.5) {
          fn({ active: true });
        } else {
          fn({ active: false });
        }
      });
    },
    {
      root: null,
      rootMargin: "0px",
      threshold: 0.5,
      ...options,
    }
  );

export function useIntersectionObserver(targetRef, callback, options = {}) {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const observer = useMemo(() => createObserver(callback, options), []);

  useEffect(() => {
    const target = targetRef.current;

    if (target) {
      observer.observe(target);
    }

    return () => {
      if (target) {
        observer.unobserve(target);
      }
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}
