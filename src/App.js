import React, { useCallback, useRef, useState } from "react";
import "./App.css";
import ExampleForm from "./components/ExampleForm";
import Gallery from "./components/Gallery";
import Heading from "./components/Heading";
import InfiniteLoader from "./components/InfiniteLoader";
import { getImages } from "./services/Image";

const IMAGES_LIMIT = 10;

const App = () => {
  const [images, setImages] = useState([]);
  const [isDone, setIsDone] = useState(false);
  const imagesMeta = useRef({
    from: 0,
    isPending: false,
  });

  const addImages = (newImages) => {
    setImages((state) => [...state, ...newImages]);
  };

  const onIntersection = useCallback(({ active }) => {
    if (active && !imagesMeta.current.isPending) {
      imagesMeta.current.isPending = true;

      getImages(imagesMeta.current.from, IMAGES_LIMIT).then((data) => {
        if (data.length) {
          addImages(data);
          imagesMeta.current.from = imagesMeta.current.from + 10;
        } else {
          // BE Api should actually return metadata about lenght and pages of images, so it won't call for empty array.
          setIsDone(true);
        }
        imagesMeta.current.isPending = false;
      });
    }
  }, []);

  return (
    <div className="app">
      <Heading>I knew you couldn't drive</Heading>
      <Gallery images={images} />
      <InfiniteLoader done={isDone} onIntersection={onIntersection} />
      <ExampleForm />
    </div>
  );
};

export default App;
