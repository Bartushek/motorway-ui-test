export const getImages = (from, limit) =>
  fetch(`images?limit=${limit}&from=${from}`)
    .then((res) => res.json())
    .catch((error) => {
      // we should return actual error here and display it in app
      console.error("Error:", error);
      return [];
    });
