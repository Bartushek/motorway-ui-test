import React, { useCallback, useRef } from "react";
import { useIntersectionObserver } from "../../hooks/useIntersectionObserver";
import styles from "./LazyLoadedImage.module.scss";

// should be in utils
const SHARP_IMAGE_URL = "//localhost:5000/sharp-image";
const getSharpImageUrl = (url, size = 1000) => {
  return `${SHARP_IMAGE_URL}?url=${encodeURIComponent(url)}-w${size}.webp`;
};

const LazyLoadedImage = ({ url, meta }) => {
  const elRef = useRef(null);
  const sourceRef = useRef(null);
  const isLoaded = useRef(false);

  const onIntersection = useCallback(({ active }) => {
    if (sourceRef.current && active && !isLoaded.current) {
      isLoaded.current = true;
      sourceRef.current.srcset = `
      ${getSharpImageUrl(url, 385)} 385w,
      ${getSharpImageUrl(url, 770)} 770w,
      `;
    }
  }, []);

  useIntersectionObserver(elRef, onIntersection, { rootMargin: "200px" });

  return (
    <div className={styles.aspectRatio}>
      <div className={styles.content}>
        <picture
          ref={elRef}
          loading="lazy"
          className={styles.picture}
          style={{ backgroundImage: `url(${meta.placeholder})` }}
        >
          <source
            ref={sourceRef}
            type="image/webp"
            sizes="
   (min-width: 640px) 770px,
   100vw 
"
          />
          {/* width and height should have original image values, but I don't have more time to do that */}
          <img
            src={meta.placeholder}
            data-src={`${url}.jpg`}
            alt="Your alt"
            className={styles.image}
            width={meta.width}
            height={meta.height}
          />
        </picture>
      </div>
    </div>
  );
};

export default LazyLoadedImage;
