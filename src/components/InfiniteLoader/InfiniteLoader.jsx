import clsx from "clsx";
import React, { useRef } from "react";
import { useIntersectionObserver } from "../../hooks/useIntersectionObserver";
import Spinner from "../Spinner";
import styles from "./InfiniteLoader.module.scss";

const InfiniteLoader = ({ onIntersection, done }) => {
  const elRef = useRef(null);

  // After it's done, it should actually unobserve intersection
  useIntersectionObserver(elRef, onIntersection);

  return (
    <div ref={elRef} className={clsx(styles.loader, done && styles.done)}>
      <Spinner />
    </div>
  );
};

export default InfiniteLoader;
