import { Field } from "formik";
import React from "react";

const Input = ({ type = 'text', name, value, error, validate }) => {
  return (
    <div>
      {name}:{" "}
      <Field
        type={type}
        value={value}
        name={name}
        validate={validate}
      />
      {error && <span>{error}</span>}
    </div>
  );
};

export default Input;
