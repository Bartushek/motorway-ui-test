import React from "react";
import { Formik } from "formik";
import Input from "./Input";
import styles from "./ExampleForm.module.scss";

const composeValidators = (validators) => {
  return (val) => {
    for (let validator of validators) {
      const result = validator(val);
      if (result) {
        return result;
      }
    }
  };
};

const validateEmailRegex = (value) => {
  return (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) &&
    "Invalid email address"
  );
};

const validateRequired = (value) => {
  return !value && "This field is required";
};

const emailValidator = composeValidators([
  validateRequired,
  validateEmailRegex,
]);

const ExampleForm = () => {
  return (
    <div className={styles.container}>
      <h2>Some unstyled form</h2>
      <Formik
        initialValues={{
          name: "",
          birthdate: "",
          color: "",
          salary: "",
          email: "",
        }}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            console.log(values);
            actions.setSubmitting(false);
          }, 1000);
        }}
      >
        {(props) => (
          <form
            onSubmit={props.handleSubmit}
            noValidate
            className={styles.form}
          >
            <Input
              type="text"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.name}
              name="name"
              error={props.errors.name}
              validate={validateRequired}
            />
            <Input
              type="email"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.email}
              name="email"
              error={props.errors.email}
              validate={emailValidator}
            />
            <Input
              type="date"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.birthdate}
              name="birthdate"
              error={props.errors.birthdate}
              validate={validateRequired}
            />
            <Input
              type="text"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.color}
              name="color"
              error={props.errors.color}
              validate={validateRequired}
            />
            <Input
              type="number"
              onChange={props.handleChange}
              onBlur={props.handleBlur}
              value={props.values.salary}
              name="salary"
              error={props.errors.salary}
              validate={validateRequired}
            />
            <button type="submit">Submit</button>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default ExampleForm;
