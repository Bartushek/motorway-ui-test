import React from "react";
import Masonry from "react-masonry-css";

import LazyLoadedImage from "../LazyLoadedImage/LazyLoadedImage";
import styles from "./Gallery.module.scss";

const columns = {
  default: 3,
  768: 2,
  425: 1,
};

const Gallery = ({ images }) => {
  return (
    <div className={styles.gallery}>
      <Masonry
        breakpointCols={columns}
        className={styles.masonry}
        columnClassName={styles.column}
      >
        {images &&
          images.map((img) => (
            <div key={img.id} className={styles.item}>
              <LazyLoadedImage
                url={img.url}
                meta={img.meta}
                alt={img.alt_description}
              />
            </div>
          ))}
      </Masonry>
    </div>
  );
};

export default Gallery;
